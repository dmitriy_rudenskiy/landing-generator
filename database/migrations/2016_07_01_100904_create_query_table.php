<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQueryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_query', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->boolean('confirmed')->default(false);
            $table->string('text');
            $table->timestamps();

            $table->unique('text');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('upload_query');
    }
}
