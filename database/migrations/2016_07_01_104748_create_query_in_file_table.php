<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQueryInFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_query_in_file', function (Blueprint $table) {
            $table->integer('file_id')->unsigned();
            $table->integer('query_id')->unsigned();

            $table->foreign('file_id')->references('id')->on('upload_file');
            $table->foreign('query_id')->references('id')->on('upload_query');

            $table->primary(['file_id', 'query_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('upload_query_in_file');
    }
}
