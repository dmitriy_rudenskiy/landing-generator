<?php
namespace App\Http\Controllers\Admin;

use App\Repositories\QueryRepository;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class QueryController extends Controller
{
    const COUNT_IN_PAGE = 15;

    public function index(QueryRepository $repository, Request $request)
    {
        $query = $request->get('query', '');

        if (!empty($query)) {
            $list = $repository->findWhere('text', 'like', '%' . $query . '%')
                ->orderBy('text')
                ->paginate(self::COUNT_IN_PAGE)
                ->appends(['text' => $query]);
        } else {
            $list = $repository->paginate(self::COUNT_IN_PAGE);
        }

        return view('admin.query.index', ['list' => $list]);
    }
}