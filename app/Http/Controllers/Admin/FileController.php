<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\FileRepository;
use App\Repositories\QueryInFileRepository;
use App\Repositories\QueryRepository;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use InvalidArgumentException;

class FileController extends Controller
{
    const COUNT_IN_PAGE = 15;

    public function index(FileRepository $repository)
    {
        $list = $repository->paginate();

        return view(
            'admin.file.index',
            [
                'list' => $list
            ]
        );
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        $list = [];

        return view('admin.file.add', ['list' => $list]);
    }

    public function view(FileRepository $repository, $id)
    {
        $file = $repository->find($id);
        $list = $file->queries()->paginate(self::COUNT_IN_PAGE);

        return view(
            'admin.file.view',
            [
                'file' => $file,
                'list' => $list
            ]
        );
    }

    public function upload(Request $request, FileRepository $fileRepository)
    {
        /* @var \Illuminate\Http\UploadedFile $upload */
        $upload = $request->file('file');

        if (!is_readable($upload->getRealPath())) {
            throw new InvalidArgumentException();
        }

        $fileEntity = $fileRepository->add($upload->getClientOriginalName(), null);

        if (!empty($fileEntity->id)) {
            $this->readFile($upload->getRealPath(), $fileEntity->id);
        }

        return redirect()->route('admin_files_view', ['id' => $fileEntity]);
    }

    public function confirm(QueryRepository $queryRepository)
    {
        $list = $queryRepository->all();

        return view('admin.file.confirm', ['list' => $list]);
    }

    protected function addQuery($text)
    {
        $queryRepository = new QueryRepository(app());

        $queryEntity = $queryRepository->get($text);

        if ($queryEntity !== null) {
            return $queryEntity;
        }

        return $queryRepository->add($text);
    }

    /**
     * Читаем загруженный файл.
     *
     * @param string $path
     * @param int $fileId
     */
    protected function readFile($path, $fileId)
    {
        $queryInFileRepository = new QueryInFileRepository(app());

        $file = new \SplFileObject($path);
        $file->setCsvControl(',');
        $file->setFlags(\SplFileObject::READ_CSV);

        // один сайт
        foreach ($file as $row) {
            if (!empty($row[1]) && (empty($row[0]) || $row[0] == "1")) {
                $queryEntity = $this->addQuery($row[1]);

                // добавляем связь файл <-> запрос
                $queryInFileRepository->add($fileId , $queryEntity->id);
            }
        }
    }
}