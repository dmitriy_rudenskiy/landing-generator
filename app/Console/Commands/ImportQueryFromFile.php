<?php
namespace App\Console\Commands;

use App\Models\Query;
use Illuminate\Console\Command;

class ImportQueryFromFile extends Command
{
    const SOURCE_ID = 3;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:query:from:files';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set('memory_limit', '-1');

        $filename = storage_path('import/query') . DIRECTORY_SEPARATOR . 'url_for_izusu.ru.csv';

        $queries = $this->loadFromFiles($filename);

        foreach ($queries as $value) {
            $this->add($value);
        }
    }

    /**
     * @param string $filename
     * @return array
     */
    protected function loadFromFiles($filename)
    {
        if (!file_exists($filename)) {
            throw new \RuntimeException();
        }

        $result = [];

        $file = new \SplFileObject($filename);

        $file->setFlags(\SplFileObject::READ_CSV);
        foreach ($file as $row) {

            if (sizeof($row) > 2) {
                list($id, $name, $alias) = $row;

                $result[] = [
                    'id' => (int)$id,
                    'name' => $name,
                    'alias' => $alias
                ];
            }

        }

        return $result;
    }

    protected function add(array $data)
    {
        $model = new Query();

        // повторяется текст запроса
        if ($model->isDuplicateName($data['name'])) {
            $this->info("\tDuplicate name\n" . implode("\n", $data));
            return false;
        }

        // повторяется псевдоним запроса
        if ($model->isDuplicateAlias($data['alias'])) {
            // $this->info("\tDuplicate alias\n" . implode("\n", $data));

            $data['alias'] .= '_' . rand(1,50);

            if ($model->isDuplicateAlias($data['alias'])) {
                return false;
            }
        }

        $data['source_id'] = self::SOURCE_ID;

        Query::forceCreate($data);
    }
}

