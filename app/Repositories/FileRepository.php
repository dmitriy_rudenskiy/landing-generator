<?php

namespace App\Repositories;

use App\Entities\File;
use Prettus\Repository\Eloquent\BaseRepository;

class FileRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return File::class;
    }

    public function add($name, $userId)
    {
        $data = [
            'name' => $name,
            'user_id' => $userId
        ];

        return $this->create($data);
    }
}