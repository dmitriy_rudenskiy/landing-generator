<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class QueryInFiles extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['file_id', 'query_id'];

    protected $table = 'upload_query_in_file';

    public $timestamps = false;
}