<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class File extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = ['user_id', 'name'];

    protected $table = 'upload_file';

    public function queries()
    {
        return $this->belongsToMany(Query::class, 'upload_query_in_file', 'file_id', 'query_id');
    }
}
