<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Query extends Model implements Transformable
{
    use TransformableTrait;

    protected $fillable = [
        'confirmed',
        'file_id',
        'maker_id',
        'model_id',
        'part_id',
        'text'
    ];

    protected $table = 'upload_query';
}
